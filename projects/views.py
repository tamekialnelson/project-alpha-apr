from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import ProjectForm

# Create your views here.


@login_required
def project_list(request):
    list = Project.objects.filter(owner=request.user)
    context = {
        "list_projects": list,
    }
    return render(request, "projects/list.html", context)


@login_required
def project_detail(request, id):
    project = get_object_or_404(Project, id=id)
    context = {"show_project": project}
    return render(request, "projects/project_detail.html", context)


@login_required
def project_create(request):
    if request.method == "GET":
        form = ProjectForm()
    else:
        form = ProjectForm(request.POST)
        if form.is_valid():
            project = form.save(False)
            project.owner = request.user
            project.save()
            return redirect("list_projects")
    context = {
        "form": form,
    }
    return render(request, "projects/create.html", context)
