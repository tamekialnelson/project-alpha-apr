from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task


# Create your views here.
@login_required
def create_task(request):
    if request.method == "GET":
        form = TaskForm()
    else:
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    context = {
        "form": form,
    }
    return render(request, "tasks/create.html", context)


@login_required
def show_tasks(request):
    list = Task.objects.filter(assignee=request.user)
    context = {
        "my_task": list,
    }
    return render(request, "tasks/my_task.html", context)
